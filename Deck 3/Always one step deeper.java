    public void run() {
        // for-Schleife ist sehr praktisch, wenn man einen bestimmten Bereich abzählen muss
        // vom Startwert bis zum Endwert kann dabei leicht durchgezählt werden.
        // 0 -> move(),
        // 1 -> turnLeft();
        // 2 -> turnRight();
        // 3 -> pickUp();
        
        String gameSteps = "002000030110000020020000030110000002002000000301100000002002000000030";
        
        
        for (int i = 0; i < gameSteps.length(); i ++){
            char step = gameSteps.charAt(i);
            switch (step){
                case '0':
                    move();
                    break;
                case '1':
                    turnLeft();
                    break;
                case '2':
                    turnRight();
                    break;
                case '3':
                    pickUp();
                    break;
                
            }
        }