public void run() {
        // Eine Schleife... ob das was mit der Implementierung zu tun hat?
        // 0 -> move(),
        // 1 -> turnLeft();
        // 2 -> turnRight();
        // 3 -> pickUp();
        
        String gameSteps = "0001000000020000200020000000100";
        
        
        for (int i = 0; i < gameSteps.length(); i ++){
            char step = gameSteps.charAt(i);
            switch (step){
                case '0':
                    move();
                    break;
                case '1':
                    turnLeft();
                    break;
                case '2':
                    turnRight();
                    break;
                
            }
        }
    }