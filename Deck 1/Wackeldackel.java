public void run() {
		// Die Energiezellen sind nur mit sehr effizientem Programmieren sammelbar
		// Dein Code hier:
		for (int i = 0; i < 4; i++){
		    move();
		}
		
        String moves = "";
        
        while (isMovePossible() == false){
            moves = "110110";
            for (int i = 0; i < moves.length(); i ++){
                char c = moves.charAt(i);
                switch(c){
                    case '1':
                        turnLeft();
                        break;
                    case '0':
                        move();
                        break;
                }
            }
        }
        moves = "00210211010";
        for (int i = 0; i < moves.length(); i++){
            char c = moves.charAt(i);
            switch(c){
                    case '1':
                        turnLeft();
                        break;
                    case '0':
                        move();
                        break;
                    case '2':
                        pickUp();
                        break;
                }
            
        }
        
        
        
        
        
    }