import codescape.Dogbot;
public class MyDogbot extends Dogbot {

    public void run() {
	    // Einlesen des Strings und Bewegung des RB zum Beginn des Labyrinths
	    move();
		turnLeft();
		/**
		 * gelesen wird ein Wort, das aus den Buchstaben "L", "M" und "R" besteht
		 * getrennt werden die Buchstaben durch ein Komma
		 * die Variable "weg" könnte so aussehen: "M,L,M,M,R,M"
		 */
		String weg = read();
		for (int i=0; i<2; i++) {
			turnRight();
		}
		
		String[] path = weg.split(",");
		
		for(String moves : path){
		    switch (moves){
		        case "L":
		            turnLeft();
		            break;
		        case "R":
		            turnRight();
		            break;
		        case "M":
		            move();
		            break;
		    }
		}
		
		move();
		turnLeft();
		move();
	    
    }
}