import codescape.Dogbot;
public class MyDogbot extends Dogbot {
/**
 * Der Funktion run() wird ein Paramter überreicht, 
 * der zum Lösen des Raums benötigt wird.
 * @param nr Nummer des Teleporters, der zum Ausgang führt
 */
    public void run(int nr) {
		// Dein Code hier:
		System.out.print(nr);
		switch (nr) {
		    case 1:
		        moves("00100020010022030");
		        break;
		    case 2:
		        moves("001000200200030");
		    case 3:
		        moves("00200010010022030");
		    case 4:
		        moves("002000100200030");
		}
		   
		

    }
    void moves(String steps){
        String moves = steps;
        for (int i = 0; i < moves.length(); i ++){
            char step = moves.charAt(i);
            switch (step){
                case '0':
                    move();
                    break;
                case '1':
                    turnLeft();
                    break;
                case '2':
                    turnRight();
                    break;
                case '3':
                    pickUp();
                    break;
            }
        }
    }


}