import codescape.Dogbot;
public class MyDogbot extends Dogbot {
/**
 * Der Funktion run() wird ein Paramter überreicht
 * @param keys Array mit Passwort-Strings für die vier Konsolen
 */
    public void run(String[] keys) {
		// Dein Code hier:
		/* Zahlen:
		* 0 -> move();
		* 1 -> turnLeft();
		* 2 -> turnRight();
        */
		moves("0001");
		write(keys[0]);
		moves("2001");
		write(keys[1]);
		moves("2002000202");
		write(keys[2]);
		moves("1002");
		write(keys[3]);
		moves("10000"); 
    }
    
    void moves(String steps){
        String moves = steps;
        for (int i = 0; i < moves.length(); i ++){
            char step = moves.charAt(i);
            switch (step){
                case '0':
                    move();
                    break;
                case '1':
                    turnLeft();
                    break;
                case '2':
                    turnRight();
                    break;
                case '3':
                    pickUp();
                    break;
            }
        }
    }
}