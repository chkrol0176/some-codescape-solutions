public void run() {
		// Die Energiezelle ist ohne weitere Kenntnisse noch nicht erreichbar
		// Dein Code hier:
		
		/*
		* 0 -> move();
		* 1 -> turnLeft();
		* 2 -> turnRight();
		* 3 -> pickUp();
		*/
		for(int i = 0; i < 3; i ++){
		    move();
		}
		String moves = "";
		while(isMovePossible() == false){
		    moves = "110110";
		    for(int i = 0; i < moves.length(); i ++){
		        char c = moves.charAt(i);
		        switch(c){
		            case '1':
		             turnLeft();
		                break;
		            case '0':
		                move();
		                break;
		        }
		    }
		}
		
		moves = "00020322020";
		
		for(int i = 0; i < moves.length(); i ++){
		    char c = moves.charAt(i);
		    switch(c){
		        case '2':
		            turnRight();
		            break;
		        case '0':
		            move();
		            break;
		        case '3':
		            pickUp();
		            break;
		    }
		}
    }